<?php

require __DIR__ . '/yieldFunctions.php';

/* ------ how to create a generator by function ------ */
$gen = false_yield_func1();
echo 'false_yield_func1 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func1();
echo 'yield_func1 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func2(false);
echo 'yield_func2 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func3(false);
echo 'yield_func3 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func4();
echo 'yield_func4 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func5();
echo 'yield_func5 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func6();
echo 'yield_func6 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_func8();
echo 'yield_func8 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

// 以下4代码运行会导致不可捕获的报错
//try {
//    $gen = yield_func11();
//} catch (\Exception $e) {
//    print_r($e->getMessage() . PHP_EOL);
//}
//echo 'yield_func11 is PHP Generator :';
//var_export($gen instanceof Generator);
//echo PHP_EOL . PHP_EOL;

$obj = new YieldClass1();
$gen = $obj->yield_method1();
echo 'YieldCalss1->yield_method1 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL;
$gen = YieldClass1::yield_method2();
echo 'YieldClass1::yield_method2 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = call_user_func('yield_func4');
echo 'call_user_func yield_func4 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = call_user_func_array('yield_func4', array());
echo 'call_user_func_array yield_func4 is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$funcName = 'yield_func12';
$gen = $funcName();
echo 'call "yield_func12" is PHP Generator :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;