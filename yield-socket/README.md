
# PHP 协程socket服务 Demo

## 安装

把代码下载来就行了。

## 运行

1. 拉起3个第三方服务
```shell script
## 启动一个处理耗时2s的库存服务
$ php ./other_server.php 8081 inventory 2

## 启动一个处理耗时4s的产品服务
$ php ./other_server.php 8082 product 4

## 监听8083端口，处理一个请求 耗时6s的 promo 服务
$ php ./other_server.php 8083 promo 6
```

2. 启动购物车服务

```shell script
## 启动一个非阻塞购物车服务
$ php ./async_cart_server.php 

## 或者启动一个一般购物车服务
$ php ./cart_server.php 
```
> 以上两个服务不能同时启用，可以交替运行，观察两种模式的效果。

3. 发起用户请求 
```shell script
$ php ./user_client.php
```

好了，看命令行输出中`user_client`的请求耗时，就知道非阻塞整个请求耗时较短，看到`async_cart_server` 输出就知道同时发起 3个非阻塞socket请求，是节约耗时的关键。。
