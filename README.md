# PHP Generator Yield Demo

#### 介绍
PHP 生成器 yield 语法Demo

yield-thread 文件夹内是一个用 php yield 参照Java Thread 写的一个 类多线程调度器

yield-scheduler 文件夹内是 [在PHP中使用协程实现多任务调度](https://www.laruence.com/2015/05/28/3038.html)  文章中部分代码的整理。

当前目录的代码，是对 `yield` 的基础用法的探究。


#### 使用说明

* PHP >= 5.5

```shell
## 运行可得什么才算得上是 生成器
$ php ./whatIsGenerator.php

## 生成器的 方法 探究
$ php ./generatorMethod.php

```

